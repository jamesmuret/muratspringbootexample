package ru.maxidom.fileproxyserver.services;


import org.springframework.http.ResponseEntity;
import ru.maxidom.fileproxyserver.components.FileUtil;
import ru.maxidom.fileproxyserver.components.FileUtilDownload;

import java.io.IOException;


public interface FileDownloadService {
    public ResponseEntity<FileUtilDownload> downloadFile(String fileName, String range, String contentTypePrefix) throws Exception;

    public ResponseEntity<FileUtil> getFileInfo(String fileName) throws IOException;
}
