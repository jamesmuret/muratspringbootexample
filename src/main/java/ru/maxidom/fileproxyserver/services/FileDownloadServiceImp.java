package ru.maxidom.fileproxyserver.services;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.maxidom.fileproxyserver.FileProxyServerApplication;
import ru.maxidom.fileproxyserver.components.FileUtil;
import ru.maxidom.fileproxyserver.components.FileUtilDownload;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

@Primary
@Service
public class FileDownloadServiceImp implements FileDownloadService {

    Logger fileServiceLog = LoggerFactory.getLogger(FileProxyServerApplication.class);
    public static final int BYTE_RANGE = 128 * 128;
    @Value("${baseDirectory}")
    private String baseDirectory;
    @Value("${subfolder.length:4}")
    private Integer subFolderLength;

    @Override
    public ResponseEntity<FileUtilDownload> downloadFile(String fileName, String range, String contentTypePrefix) throws Exception {
        validateFileNameLength(fileName);
        FileUtilDownload fudResponse = new FileUtilDownload();
        fudResponse.setFileName(fileName);
        fudResponse.setEncoding("base64 - binary");
        Path fileLocation = Paths.get(baseDirectory + File.separator + fileName.substring(0, subFolderLength).toLowerCase() + File.separator + fileName);
        long rangeStart = 0;
        long rangeEnd;
        byte[] data;
        long fileSize;
        String fileType = fileName.substring(fileName.lastIndexOf(".") + 1);
        fileSize = Optional.ofNullable(fileLocation)
                .map(this::sizeFromFile)
                .orElse(0L);
        if (range == null || range.isEmpty()) {
            data = readByteRange(fileLocation, rangeStart, fileSize - 1);
            fudResponse.setFileBody(new String(data,StandardCharsets.UTF_8));
            fudResponse.setByteRange((long) data.length);
            return ResponseEntity.status(HttpStatus.OK).body(fudResponse);
        }
        String[] ranges = range.split("-");
        rangeStart = Long.parseLong(ranges[0].substring(6));
        if (ranges.length > 1) {
            rangeEnd = Long.parseLong(ranges[1]);
        } else {
            rangeEnd = fileSize - 1;
        }
        if (fileSize < rangeEnd) {
            rangeEnd = fileSize - 1;
        }
        data = readByteRange(fileLocation, rangeStart, rangeEnd);
        fudResponse.setFileBody(new String(data, StandardCharsets.UTF_8));
        fudResponse.setByteRange((long) data.length);
        return ResponseEntity.status(HttpStatus.OK).body(fudResponse);
    }

    private void validateFileNameLength(String fileName) {
        fileServiceLog.debug("Длина файла {}", fileName.length());
        if (fileName == null || fileName.isEmpty() || fileName.length() < subFolderLength) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Минимальная длина имени файла - {}" + subFolderLength);
        }
    }

    @Override
    public ResponseEntity<FileUtil> getFileInfo(String fileName) throws IOException {
        validateFileNameLength(fileName);
        fileServiceLog.debug("Путь к файлу {}", baseDirectory + File.separator + fileName.substring(0, subFolderLength).toLowerCase() + File.separator + fileName);
        Path filePath = Paths.get(baseDirectory + File.separator + fileName.substring(0, subFolderLength).toLowerCase() + File.separator + fileName);
        FileUtil responseFile = new FileUtil();
        if (Files.exists(filePath)) {
            fileServiceLog.debug("Файл по имени уже существует");
            responseFile.setFileName(fileName);
            fileServiceLog.debug("Размер файла  {}", Files.size(filePath));
            responseFile.setLength(Files.size(filePath));
        } else {
            fileServiceLog.debug("Запрашиваемый файл не существует {}", responseFile.getFileName());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        return ResponseEntity.status(HttpStatus.OK).body(responseFile);
    }

    private byte[] readByteRange(Path location, long start, long end) throws IOException {
        try (InputStream inputStream = (Files.newInputStream(location));
             ByteArrayOutputStream bufferedOutputStream = new ByteArrayOutputStream()) {
            byte[] data = new byte[BYTE_RANGE];
            int nRead;
            while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
                bufferedOutputStream.write(data, 0, nRead);
            }
            bufferedOutputStream.flush();
            byte[] result = new byte[(int) (end - start) + 1];
            System.arraycopy(bufferedOutputStream.toByteArray(), (int) start, result, 0, result.length);
            return java.util.Base64.getEncoder().encode(result);
        }
    }

    private Long sizeFromFile(Path path) {
        try {
            return Files.size(path);
        } catch (IOException ex) {
            fileServiceLog.warn(ex.getMessage());
            fileServiceLog.warn(ExceptionUtils.getStackTrace(ex));
        }
        return 0L;
    }
}
