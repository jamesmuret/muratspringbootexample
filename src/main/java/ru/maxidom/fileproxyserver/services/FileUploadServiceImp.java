package ru.maxidom.fileproxyserver.services;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.maxidom.fileproxyserver.components.FileUtil;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Set;

@Service
public class FileUploadServiceImp implements FileUploadService {

    FileUploadServiceImp(@Value("${fileSubDirPosixPermissions:rwxrwx---}") String fileSubDirPosixPermissions ){
        perms = PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString(fileSubDirPosixPermissions));
    }
    private  FileAttribute<Set<PosixFilePermission>> perms;
    @Value("${hashingAlg.name:MD5}")
    private String hashAlgName;
    @Value("${subfolder.length:4}")
    private Integer subFolderLength;
    @Value("${baseDirectory}")
    private String baseDirectory;
    @Value("${tmpDirectory}")
    private String tmpDirectory;
    Logger fileUploadServiceLog = LoggerFactory.getLogger(FileUploadServiceImp.class);

    private String calculatehashchecksumMd5(byte[] multipartFileHash) throws NoSuchAlgorithmException {
        if (multipartFileHash == null) {
            throw new IllegalArgumentException("Файл пустой");
        }
        fileUploadServiceLog.debug("Подсчет хеша ");
        MessageDigest messageDigest = MessageDigest.getInstance(hashAlgName);
        fileUploadServiceLog.debug("MessageDigest init {}", messageDigest.getAlgorithm());
        messageDigest.update(multipartFileHash);
        fileUploadServiceLog.debug("read all bytes");
        byte[] hash = messageDigest.digest();
        StringBuilder hex = new StringBuilder();
        for (byte b : hash) {
            hex.append(String.format("%02x", b));
        }
        return hex.toString().toLowerCase();
    }

    @Override
    public ResponseEntity<FileUtil> uploadFile(MultipartFile multipartFile) throws NoSuchAlgorithmException, IOException {
        if (multipartFile == null) {
            throw new IllegalArgumentException("Файл пустой");
        }

        String hexHashFileName = calculatehashchecksumMd5(multipartFile.getBytes());
        fileUploadServiceLog.debug("Хеш файла {}", hexHashFileName);
        FileUtil responseFile = new FileUtil();
        String extension = "";
        int index = multipartFile.getOriginalFilename().toString().lastIndexOf('.');
        if (index > 0) {
            extension = "." + multipartFile.getOriginalFilename().toString().substring(index + 1);
        }
        Path hexDirPath = Paths.get(baseDirectory + File.separator + hexHashFileName.substring(0, subFolderLength).toLowerCase());
        Path hexFilePath = Paths.get(hexDirPath + File.separator + hexHashFileName + extension);
        responseFile.setFileName(hexFilePath.getFileName().toString());
        responseFile.setLength((long) multipartFile.getBytes().length);
        fileUploadServiceLog.debug("Путь к хешированному файлу {}", hexFilePath);
        try {
            Files.createDirectory(hexDirPath);
            Files.setPosixFilePermissions(hexDirPath,perms.value());
        } catch (IOException ie) {
            if (!(ie instanceof FileAlreadyExistsException)) {
                throw ie;
            }
            fileUploadServiceLog.debug("Директория существует");
        }
        if (Files.exists(hexFilePath)) {
            fileUploadServiceLog.debug("Такой файл уже существует");
            return ResponseEntity.status(HttpStatus.OK).body(responseFile);
        }
        fileUploadServiceLog.debug("Файл не найден в списке существующих, атомарное создание заново");
        try {
            Files.write(hexFilePath, multipartFile.getBytes(), StandardOpenOption.CREATE_NEW);
            Files.setPosixFilePermissions(hexFilePath,perms.value());
        } catch (IOException ie) {
            if (!(ie instanceof FileAlreadyExistsException)) {
                throw ie;
            }
            fileUploadServiceLog.warn("Файл {} уже существует", hexHashFileName);
        }
        return ResponseEntity.status(HttpStatus.OK).body(responseFile);
    }
}
