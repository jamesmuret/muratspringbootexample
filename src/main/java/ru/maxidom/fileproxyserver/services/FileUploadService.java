package ru.maxidom.fileproxyserver.services;


import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import ru.maxidom.fileproxyserver.components.FileUtil;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public interface FileUploadService {
    ResponseEntity<FileUtil> uploadFile(MultipartFile multipart) throws NoSuchAlgorithmException, IOException;
}
