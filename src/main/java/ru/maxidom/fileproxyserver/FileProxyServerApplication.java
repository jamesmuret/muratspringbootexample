package ru.maxidom.fileproxyserver;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@SpringBootApplication
public class FileProxyServerApplication implements CommandLineRunner {
    Logger appLog = LoggerFactory.getLogger(FileProxyServerApplication.class);
    @Value("${baseDirectory}")
    private String baseDirectory;
    @Value("${tmpDirectory}")
    private String tmpDirectory;


    public static void main(String[] args) {
        SpringApplication.run(FileProxyServerApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        try {
            appLog.info("FileProxyServer инициализация");
            Path tmpFolderPath = Paths.get(tmpDirectory);
            Path baseCatalogPath = Paths.get(baseDirectory);
            if (Files.notExists(baseCatalogPath) || !Files.isReadable(baseCatalogPath) || !Files.isWritable(baseCatalogPath)) {
                appLog.error("Директория с файлами {} не найдена или не доступна",baseCatalogPath);
                System.exit(0);
            }
            else if(Files.notExists(tmpFolderPath) || !Files.isReadable(tmpFolderPath) || !Files.isWritable(tmpFolderPath)){
                appLog.error("Временная директория {} не найдена или не доступна", tmpFolderPath);
                System.exit(0);
            }
            appLog.info("Временная директория {} - очистка",tmpFolderPath);
            FileUtils.cleanDirectory(tmpFolderPath.toFile());
            appLog.info("Инициализация завершена.Запуск приложения FileProxyServer");
        } catch (IOException exp) {
            appLog.error("Невозможно запустить сервер ошибка - {}", exp.getMessage());
            appLog.error(ExceptionUtils.getStackTrace(exp));
            System.exit(0);
        }
    }
}
