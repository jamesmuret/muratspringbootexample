package ru.maxidom.fileproxyserver.controllers;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import ru.maxidom.fileproxyserver.FileProxyServerApplication;
import ru.maxidom.fileproxyserver.components.FileUtil;
import ru.maxidom.fileproxyserver.components.FileUtilDownload;
import ru.maxidom.fileproxyserver.services.FileDownloadService;
import ru.maxidom.fileproxyserver.services.FileUploadService;


@RestController()
@RequestMapping("/api/v1")
public class FileProxyApiController {

    Logger fileControllerLog = LoggerFactory.getLogger(FileProxyServerApplication.class);
    @Value("${file.contentPrefix}")
    private String contentTypePrefix;
    @Autowired
    FileDownloadService downloadFileService;
    @Autowired
    FileUploadService uploadFileService;

    @GetMapping("/download/{filename}")
    public ResponseEntity<FileUtilDownload> downloadFile(@PathVariable("filename") String fileName, @RequestParam(defaultValue = "", required = false) String range, @RequestParam(required = true) String userid) {
        try {
            fileControllerLog.info("Скачивание файла " + fileName + " диапазон " + range + " пользователь " + userid);
            return downloadFileService.downloadFile(fileName, range, contentTypePrefix);
        } catch (Exception exp) {
            fileControllerLog.error(exp.getMessage());
            fileControllerLog.error(ExceptionUtils.getStackTrace(exp));
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, exp.getMessage());
        }
    }

    @PostMapping("/upload/")
    public ResponseEntity<FileUtil> uploadFile(@RequestParam(value = "file") MultipartFile uploadFile, @RequestParam(required = true) String userid) {
        try {
            fileControllerLog.info("Загрузка файла " + uploadFile.getName() + " пользователь" + userid + " размер файла " + uploadFile.getSize());
            return uploadFileService.uploadFile(uploadFile);
        } catch (Exception exp) {
            fileControllerLog.error(exp.getMessage());
            fileControllerLog.error(ExceptionUtils.getStackTrace(exp));
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, exp.getMessage());
        }
    }

    @GetMapping("/fileinfo/{fileName}")
    public ResponseEntity<FileUtil> getFileInfo(@PathVariable("fileName") String fileName) {
        try {
            fileControllerLog.info("Запрос мета данных файла {}", fileName);
            return downloadFileService.getFileInfo(fileName);
        } catch (Exception exp) {
            fileControllerLog.error(exp.getMessage());
            fileControllerLog.error(ExceptionUtils.getStackTrace(exp));
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, exp.getMessage());
        }
    }
}
