package ru.maxidom.fileproxyserver.components;


public class FileUtilDownload {

    private String encoding;
    private String fileBody;
    private Long byteRange;
    private String fileName;

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public String getFileBody() {
        return fileBody;
    }

    public void setFileBody(String fileBody) {
        this.fileBody = fileBody;
    }

    public Long getByteRange() {
        return byteRange;
    }

    public void setByteRange(Long byteRange) {
        this.byteRange = byteRange;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
