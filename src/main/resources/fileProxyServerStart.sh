#!/bin/bash
export applicationJar=$1
export baseDir=/apps/fileproxyserver
export logDir=$baseDir/log
export cmd="(ps -ef | grep $baseDir/${applicationJar} | grep -v grep | grep -v bash| grep -v crond | awk '{print \$2}')"
eval pid='$'$cmd
if [ "$applicationJar" = "" ] 
then 
	echo 'empty application name'
else
        whoami
	mkdir -p $logDir
	chmod -R 770 $logDir
	if [ "$pid" = "" ]
	then 
		export applicationPropertiesFile=$baseDir/application.properties
		echo 'server.port=3307' > $applicationPropertiesFile
		echo 'management.server.port=3308' >> $applicationPropertiesFile
		echo 'spring.main.banner-mode=off' >> $applicationPropertiesFile
		echo 'server.error.include-message=always' >> $applicationPropertiesFile
		echo 'management.endpoints.web.base-path=/monitor' >> $applicationPropertiesFile
		echo 'management.endpoints.web.exposure.include=shutdown,health' >> $applicationPropertiesFile
		echo 'management.endpoint.health.show-details=always' >> $applicationPropertiesFile
		echo 'management.endpoint.shutdown.enabled=true' >> $applicationPropertiesFile
		echo 'management.endpoint.info.enabled=true' >> $applicationPropertiesFile
	    echo 'spring.servlet.multipart.maxFileSize=-1'  >> $applicationPropertiesFile
	    echo 'spring.servlet.multipart.maxRequestSize=-1'  >> $applicationPropertiesFile
		echo 'baseDirectory='$baseDir'/pictures'  >> $applicationPropertiesFile
	    echo 'subfolder.length=4'  >> $applicationPropertiesFile
	    echo 'tmpDirectory='$baseDir'/tmp'  >> $applicationPropertiesFile
	    echo 'file.contentPrefix=image'  >> $applicationPropertiesFile
	    echo 'hashingAlg.name=MD5'  >> $applicationPropertiesFile
	    echo 'spring.servlet.multipart.location="${tmpDirectory}"'  >> $applicationPropertiesFile
	    echo 'logging.level.springframework=INFO'  >> $applicationPropertiesFile
	    echo 'logging.level.ru.maxidom=DEBUG'  >> $applicationPropertiesFile
	    echo 'logging.pattern.console="%d{yyyy-MM-dd HH:mm:ss} %-5level - %msg%n"'  >> $applicationPropertiesFile
	    echo 'logging.pattern.file=%d{yyyy-MM-dd HH:mm:ss} [%thread] %-5level %logger{36} - %msg%n'  >> $applicationPropertiesFile
	    echo 'logging.file.path='$logDir  >> $applicationPropertiesFile
	    echo 'logging.file.name='$logDir'/fps.log'  >> $applicationPropertiesFile
	    echo 'logging.charset.file=UTF-8'  >> $applicationPropertiesFile
	    echo 'logging.logback.rollingpolicy.max-file-size=5MB'  >> $applicationPropertiesFile
  
		chmod 770 $applicationPropertiesFile
		nice -n 19 java -jar $baseDir/$applicationJar --spring.config.location=file:$baseDir/ &
		echo `date "+%d/%m/%y %H:%M:%S"` " $applicationJar starting"
 	
	else
		echo  `date "+%d/%m/%y %H:%M:%S"` " $applicationJar is already started pid=$pid"
	fi
fi